import React from 'react'
import {BsLinkedin,} from 'react-icons/bs'
import {SiGitlab,} from 'react-icons/si'
import {SiWhatsapp,} from 'react-icons/si'


const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://www.linkedin.com/in/raul-azahares/" target="_blank"><BsLinkedin/></a>
        <a href="https://gitlab.com/araulmanuel" target="_blank"><SiGitlab/></a>
        <a href="https://walink.co/cfe5a7" target="_blank"><SiWhatsapp/></a>
    </div>
  )
}

export default HeaderSocials