import React from 'react'
import './header.css'
import CTA from './CTA'
import HeaderSocials from './HeaderSocials'

const Header = () => {
  return (
    <header>
      <div className="container header__container">
        <h5>Hi I'm </h5>
        <h1>Raúl Manuel Azahares Reyes </h1>
        <h4 className="text-light">Fullstack Developer</h4>
        <CTA/>
        <HeaderSocials/>
        <div className="me">
          
        </div>

        <a href="#contact" className='scroll_down'> Scroll Down </a>

      </div>
    </header>
  )
}

export default Header
