import React from "react";
import "./portafolio.css";
import IMG1 from "../../assets/portfolio1.jpg";
import IMG2 from "../../assets/portfolio2.jpg";
import IMG3 from "../../assets/portfolio3.jpg";
import IMG4 from "../../assets/portfolio4.jpg";
import IMG5 from "../../assets/portfolio5.png";
import IMG6 from "../../assets/portfolio6.jpg";
import WEEKAWAY from '../../assets/weekaway.png'
import MORES from '../../assets/mores2.png'
import BB3 from '../../assets/BB31.png'

const Portafolio = () => {
  return (
    <section id="portafolio">
      <h5>My Recent Work</h5>
      <h2>Portafolio</h2>
      <div className="container portafolio__container">
        <article className="portafolio_item">
          <div className="portafolio__item-image">
            <img src={WEEKAWAY} alt="" />
          </div>
          <h3>WeekAway</h3>
          <div className="portafolio__item-cta">
            <a href="https//gitlab.com" className="btn-disable">
              Gitlab
            </a>
            <a
              href="https://www.weekaway.fr/"
              className="btn btn-primary"
              target="__blank"
            >
              Live Production
            </a>
          </div>
        </article>
        <article className="portafolio_item">
          <div className="portafolio__item-image">
            <img src={MORES} alt="" />
          </div>
          <h3>Moresneakers</h3>
          <div className="portafolio__item-cta">
            <a href="https//gitlab.com" className="btn-disable">
              Gitlab
            </a>
            <a
              href="https://moresneakers.com/"
              className="btn btn-primary"
              target="__blank"
            >
              Live Production
            </a>
          </div>
        </article>
        <article className="portafolio_item">
          <div className="portafolio__item-image">
            <img src={BB3} alt="" />
          </div>
          <h3>Nacon Blood Bowl 3</h3>
          <div className="portafolio__item-cta">
            <a href="https//gitlab.com" className="btn-disable">
              Gitlab
            </a>
            <a
              href="https://bb3.harlow.fr/"
              className="btn btn-primary"
              target="__blank"
            >
              Live Demo
            </a>
          </div>
        </article>
      </div>
    </section>
  );
};

export default Portafolio;
