import React from "react";
import "./testimonials.css";
import AVTR1 from "../../assets/avatar1.jpg";
import AVTR2 from "../../assets/avatar2.jpg";
import AVTR3 from "../../assets/avatar3.jpg";

const Testimonials = () => {
  return (
    <section id="testimonials">
      <h5>Review from clients</h5>
      <h2>Testimonials</h2>

      <div className="container testimonials__container">
        <article className="testimonial">
          <div className="client__avatar">
            <img src={AVTR1} alt="" />
          </div>
          <h5 className="client__name">Frank</h5>
          <small className="client__review">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi
            ex cum omnis, beatae laudantium et voluptas dolor sequi fugit
            voluptatum accusantium sunt quaerat nisi, perspiciatis ea earum
            nobis. Velit, molestias?
          </small>
        </article>
        <article className="testimonial">
          <div className="client__avatar">
            <img src={AVTR1} alt="" />
          </div>
          <h5 className="client__name">Frank</h5>
          <small className="client__review">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi
            ex cum omnis, beatae laudantium et voluptas dolor sequi fugit
            voluptatum accusantium sunt quaerat nisi, perspiciatis ea earum
            nobis. Velit, molestias?
          </small>
        </article>
        <article className="testimonial">
          <div className="client__avatar">
            <img src={AVTR1} alt="" />
          </div>
          <h5 className="client__name">Frank</h5>
          <small className="client__review">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi
            ex cum omnis, beatae laudantium et voluptas dolor sequi fugit
            voluptatum accusantium sunt quaerat nisi, perspiciatis ea earum
            nobis. Velit, molestias?
          </small>
        </article>
      </div>
    </section>
  );
};

export default Testimonials;
