import React from "react";
import "./services.css";
import { FaCheck } from "react-icons/fa";

const Services = () => {
  return (
    <section id="services">
      <h5>What I offer</h5>
      <h2>Services</h2>
      <div className="container services__container">
        <article className="service">
          <div className="service__head">
            <h3>UI/UX Design</h3>
          </div>
          <ul className="service__list">
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Creating components with React and Tailwindcss.
                
              </p>
            </li>
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Mockup design
                
              </p>
            </li>
          </ul>
        </article>
        <article className="service">
          <div className="service__head">
            <h3>Web Development</h3>
          </div>
          <ul className="service__list">
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Design and implementation of API REST.
                
              </p>
            </li>
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Design, optimization and normalization of Databases.
                
              </p>
            </li>
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Specification of requirements according to the needs of the client.
                
              </p>
            </li>
          </ul>
        </article>
        <article className="service">
          <div className="service__head">
            <h3>Anothers</h3>
          </div>
          <ul className="service__list">
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Lead small teams with agile methodology.
                
              </p>
            </li>
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Implement scripts for web scraping.
                
              </p>
            </li>
            <li>
              <FaCheck className="service__list-icon" />
              <p>
              Able to create a great work environment  😊.
                
              </p>
            </li>
          </ul>
        </article>
      </div>
    </section>
  );
};

export default Services;
